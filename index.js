let firstName = 'John';
let lastName = 'Smith';
let age = 30;
let hobbies = ['Biking', 'Mountain CLimbing', 'Swimming'];
const city = 'Lincoln City';
const houseNumber = 32;
const state = 'Nebraska';
const street = 'Washington Street';
const isMarried = true;

function UserInformation() {
    console.log(`${firstName}  ${lastName}   is ${age} years of age.`)
    console.log(`${firstName} lives in ${houseNumber}, ${street}, ${city}, ${state}.`)
}

function Hobbies() {
    console.log(`His hobbies are: ${hobbies}`)
}

function RelationshipStatus() {
    console.log(`${firstName} ${lastName} marriage is: ${isMarried}`)
}